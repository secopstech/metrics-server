# Metrics Server

This forked from https://github.com/kubernetes-incubator/metrics-server/deploy since the original
config files are a bit outdated (metrics server deployment uses v0.2.0 but the latest is v0.2.1)


In order to deploy metrics server, aggregation layer should be enabled, to do this check 
https://bitbucket.org/secopstech/docs/src/metrics-server.md 

